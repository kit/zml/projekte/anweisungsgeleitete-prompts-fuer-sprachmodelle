# Antwort Validierung

LLMs sind antworten auf Basis statistischer Modelle. Sie werden zwar immer besser, dennoch kommt es zu Fehlern in den Antworten, sogenannten Halluzinationen.  Ein gewisses Maß an Halluzination ist bei allen autoregressiven LLMs unvermeidlich, da sie auf statistischen Mustern in den Trainingsdaten basieren und nicht über ein Verständnis der zugrunde liegenden Realität verfügen. Die Ursachen können unterschiedlicher Art sein:

 * Reines statistisches Verfahren, falsche Fakten: Das Modell kann Behauptungen aufstellen, die sachlich nicht korrekt sind. LLMs bauen Wortfolgen auf statistischen Zusammenhänge zwischen den Wörtern zusammen. Die Länge der jeweils betrachteten Folgen wird immer länger und verbessert dadurch die Qualität der Antworten, dennoch besteht durch das LLM kein Verständnis des Inhaltes selbst. Generell können so Fehler entstehen. Z.B. Rechnungen erfolgen nicht als Berechnung, sondern rein auf statistischer Basis möglicher Antworten auf die Aufgabe.
 * Bias: Die Trainingsdaten beinhalten bereits einen Bias, der sich dann auch in den Antworen wieder findet.
 * Veraltete Informationen: LLMs beruhen auf großen Datensätzen an Trainingsdaten. Diese werden zu einem bestimmten Zeitpunkt erfasst und das LLM auf diesen trainiert. Daten ab diesem Zeitpunkt sind dem LLM daher unbekannt. D.h. LLMs können keine Fragen zu Fakten von aktuellen Geschehnissen beantworten, sie erraten diese nur.


Daher ist eine wesentliche Aufgabe bei der Verwendung von LLMs zur Beantwortung von Fragestellungen die Reflexion und Überprüfung der Antwort. In diesem Dokument sind Ansätze gesammelt dies bereits beim Prompting zu bedenken und mittels des LLMs selbst, zumindest Plausibilitätsproben der Antworten durchzuführen.

# Konzept und Grenzen des Verfahrens
Das Vorgehen ist vergleichbar mit einer Proberechnung in der Mathematik. Wenn die ursprüngliche Rechnung richtig war, muss auch die Umkehrrechnung zu den ursprünglichen Werten führen. Etwas komplexer dürfen auch bei mehrstufiger Entwicklung von Antworten die auf vorherigen Antworten basieren auch in der Kombination dieser keine Widersprüche auftreten.

Diese Art der Üperprüfung garantiert nicht, dass die Antwort tatsächlich korrekt ist. Sie erlaubt allerdings einzuordnen, ob das Verhalten des LLMs in Bezug auf die aktuelle Fragestellung in sich widerspruchsfrei ist und entlarvt gröbere Ungereihmtheiten. Ebfalls hilft es Antworten zu plausibilisieren.

Dieser Ansatz ist auch in Dhuliawala, S., Komeili, M., Xu, J., Raileanu, R., Li, X., Celikyilmaz, A., & Weston, J. (2023). [Chain-of-Verification Reduces Hallucination in Large Language Models](https://doi.org/10.48550/arXiv.2309.11495). ArXiv, abs/2309.11495 beschrieben. 

# Methoden

## CoVe – Chain of Verification
 
Im Paper wird eine **Chain-of-Verification (CoVe)** Methode vorgeschlagen. Diese Überprüfungskette ist so angelegt, dass sie mit dem LLM selbst durchgefürht werden kann. Generell kann diese Vorgehensweise aber auch manuell ausgeführt werden. Diese besteht aus vier Schritten:

1. **Generierung einer Basisantwort**: Für eine Anfrage wird eine Antwort mit dem LLM generiert.
2. **Planung von Überprüfungen**: Für die Anfrage und die Basisantwort wird eine Liste von Überprüfungsfragen zu den in der Basisantwort genannten Details generiert, die dazu beitragen könnten, selbst zu analysieren, ob es Fehler in der ursprünglichen Antwort gibt. Das Modell kann auch dazu verwendet diese Fragen zu generieren: `Erstelle Fragen um die einzelnen Details der Antwort jeweils seperart zu überprüfen.`
3. **Durchführung der Überprüfungen**: Jede Überprüfungsfrage wird der Reihe nach beantwortet und damit die Antwort gegen die ursprüngliche Basisantwort geprüft, um auf Inkonsistenzen oder Fehler zu prüfen.
4. **Generierung einer endgültigen überprüften Antwort**: Bei Eingabe der entdeckten Inkonsistenzen (falls vorhanden) wird eine überarbeitete Antwort generiert, die die Überprüfungsergebnisse einbezieht.


Für die **praktische Durchführung** der Überprüfung werden unterschiedliche Vorgehensweisen vorgeschlagen:

1. **Joint**: Im gemeinsamen Verfahren werden die Planung und Ausführung (Schritte 2 und 3) mit einem einzigen LLM-Prompt durchgeführt, so dass die Antwort sowohl die Verifizierungsfragen als auch deren Antworten unmittelbar nach den Fragen enthält. Bei diesem Ansatz sind separate Prompts nicht erforderlich.
2. **2-Step**: Dadurch, dass die Generierung der Überprüfungsfragen und die Ausfürhung dieser in einem Prompt gebündelt sind, können manchmal die gleichen Halluzinationen wie in der Basisantwort auftreten da jeweils der gleiche Kontext verwendet wird. 2-Stept trennt die Planung und Ausführung in separate Schritte, jeweils mit einem eigenen LLM-Prompt. Der Planungsprompt erstellt die Verifizierzungsfragen im ersten Schritt. Die aus der Planung generierten Verifizierungsfragen werden im zweiten Schritt beantwortet, wobei entscheidend ist, dass der Kontext für den LLM-Prompt nur diese Fragen enthält und nicht die ursprüngliche Basisantwort und daher diese Antworten nicht direkt wiederholen kann.
3. **Factored**: Ein weiterer Ansatz besteht darin, alle Fragen unabhängig voneinander als separate Prompts zu beantworten, um das Prinzip der Unabhängigkeit der Antworten weiter zu verbessern. Wiederum entscheidend ist, dass diese Prompts nicht die ursprüngliche Basisantwort enthalten und daher nicht einfach kopieren oder wiederholen. Dieser Ansatz kann potenziell mehr Verifizierungsfragen bewältigen, da sie nicht alle in denselben einzelnen Kontext passen müssen. 
4. **Factor+Revise**: Nachdem die Verifizierungsfragen beantwortet wurden, muss die gesamte CoVe-Pipeline dann implizit oder explizit überprüfen, ob diese Antworten eine Inkonsistenz mit den ursprünglichen Antworten anzeigen. Im Ansatz Faktor+Überarbeiten wird dies als bewusst weiterer Schritt über einen zusätzlichen LLM-Prompt ausgeführt. Das System soll über diesen Schritt explizit getrennt "nachdenken". 
