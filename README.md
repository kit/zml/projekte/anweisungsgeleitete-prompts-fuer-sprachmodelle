# Anweisungsgeleitete Prompts für Sprachmodelle (ChatGPT, GPT-3.5, GPT-4)

Dieses Projekt basiert auf [Instruction-following prompts for ChatGPT, GPT-3.5, GPT-4](https://github.com/kevinamiri/Instructgpt-prompts/) von Kevin Amiri. Es stellt im wesentlichen eine Übersetzung in deutsche Sprache dar.

Dieses Projekt besteht aus Prompts für die Modelle ChatGPT und GPT-3.5, die darauf ausgelegt sind, bei Schreib-, Analyse- und Verständnisaufgaben zu helfen. Unten finden Sie zahlreiche Prompts, die Sie nutzen können, um Inhalte für Ihre Projekte zu generieren, Ihren Code zu debuggen, Lösungen für Probleme zu finden oder einfach mehr darüber zu erfahren, was diese Modelle leisten können. Durch die Verwendung der geeigneten Anweisungsverben können Sie die Modelle anleiten, jegliche sprachbezogenen Aufgaben zu lösen.

[InstructGPT](https://arxiv.org/abs/2203.02155) (in neueren Versionen als GPT-3.5 bezeichnet) sind Sprachmodelle, die durch Anweisungen und menschliches Feedback trainiert wurden, um die Absichten eines Benutzers besser zu verstehen und sich darauf abzustimmen, wodurch genauere und angemessenere Ausgaben produziert werden.

[ChatGPT](https://openai.com/blog/chatgpt/) verwendet ebenfalls die InstructGPT-Methode, aber in einer Dialogform, um Benutzeranweisungen zu verstehen und basierend auf den Anweisungen des Benutzers Ausgaben zu generieren.

[GPT4](https://openai.com/research/gpt-4)ist leistungsfähiger als jedes GPT-3.5 Modell; es kann komplexere Anweisungen handhaben und diese effektiver befolgen und anwenden.


## Warum:

Dies ist eine einfache und unkomplizierte Methode, um das Modell anzuleiten, fast alles zu tun. Sie verwendet eine einfache Struktur, um Anweisungen zu geben, und kann sich an jede sprachbezogene Aufgabe anpassen.

## Nutzung:

### Nützliche Verben:

Anweisungsverben sind das erste Wort im Prompt. Sie werden verwendet, um das Modell anzuweisen, etwas zu tun. Sie sind der wichtigste Teil des Prompts. Man könnte argumentieren, dass sie wie magische Wörter wirken, die einen erfolgreichen Prompt ausmachen, da sie helfen, dass die Maschine Ihre Anweisungen versteht und befolgt. Jedes dieser Verben hat eine spezifische Bedeutung, die dem Modell helfen kann zu verstehen, welche Art von Antwort erwartet wird. Wenn Sie das Modell beispielsweise bitten, etwas zu erklären, wird es eine detaillierte Erklärung liefern. Andererseits wird das Modell, wenn Sie es bitten, etwas zusammenzufassen, einen kurzen Überblick geben.

- <details><summary>Nützliche Wörter, um auf Abschnitte Ihres Eingabetextes hinzuweisen: (zeigen Sie auf eine Stelle Ihres Eingabetextes)</summary><p>
  - Gegebener Text
  <br>
  - folgender Abschnitt
  <br>
  - Folgendes
  <br>
  - Das Folgende
- <details><summary>Nützliche Wörter, um Anweisungen zu geben: (um dem Modell Anweisungen zu geben)</summary><p>
  - Erkläre
	<br>
  - Erstelle
	<br>
  - Generiere
	<br>
  - Gib
	<br>
  - Schreibe
	<br>
  - Fasse zusammen
	<br>
  - Paraphrasiere
	<br>
  - Übersetze
	<br>
  - Schreibe um
	<br>
  - Formuliere um
	<br>
  - Formuliere neu
	<br>
  - Liste
	<br>
  - Extrahiere
	<br>
  - Wähle aus
	<br>
  - Analysiere
	<br>
  - Definiere
	<br>
  - Identifiziere
	<br>
  - Beschreibe
	<br>
  - Sage voraus
	<br>
  - Erkläre
	<br>
  - Analysiere
	<br>
  - Vergleiche
	<br>
  - Kategorisiere
	<br>
  - Bewerte
	<br>
  - Kritisiere
	<br>
  - Differenziere
	<br>
  - Untersuche
	<br>
  - Veranschauliche
	<br>
  - Umreiße
	<br>
  - Berechne
	<br>
  - Folgere
	<br>
  - Überarbeite
	<br>
  - Fasse zusammen
	<br>
  - Klassifiziere
	<br>
  - Entwickle
	<br>
  - Bewerte
	<br>
  - Formuliere
	<br>
  - Diskutiere
	<br>
  - Kläre
	<br>
  - Hebe hervor
	<br>
  - Vereinfache
	<br>
  - Wende an
	<br>
  - Schlage vor / Mache einen Vorschlag
	<br>
  - Mache
	<br>
   * Du kannst `Kannst du` hinzufügen im Falle von ChatGPT, um es konversationeller zu machen, beides funktioniert sowohl im Instruktionsmodell als auch in ChatGPT-Modellen.
    - Kannst du geben …
    - Kannst du generieren …
</p></details>






### Häufige Anwendungsfälle:

#### Klassifizierung

Anweisungsverben, die für Klassifizierungsaufgaben in der Sprachverarbeitung nützlich sind

  - `Klassifiziere` das Folgende als eine physische oder chemische Eigenschaft
  - `Identifiziere` die Hauptidee des gegebenen Abschnitts
  - `Kategorisiere` den folgenden Abschnitt entweder als Mythos oder Fakt
  - `Weise jedem` Element basierend auf seinen Merkmalen die entsprechende Kategorie zu
  - `Liste` die fünf Hauptkategorien des gegebenen Datensatzes auf.
  - `Gruppiere` die Elemente basierend auf ihren gemeinsamen Merkmalen
  - `Sortiere` die Daten basierend auf dem Datum ihrer Erfassung
  - `Kennzeichne` jeden Datenpunkt mit seiner entsprechenden Kategorie
  - `Teile` die Daten basierend auf ihren Ähnlichkeiten in kleinere Untergruppen
  - `Gruppiere` die Elemente basierend auf ihrer Ähnlichkeit in Merkmalen
  - `Beschrifte` jede Gruppe mit einem beschreibenden Namen.

#### Generierung

Anweisungsverben, die für Generierungsaufgaben nützlich sind

  - `Generiere` einen neuen Text basierend auf den gegebenen Parametern
  - `Erstelle` eine kurze, einprägsame Überschrift basierend auf dem Folgenden, ziele darauf ab, das Produkt zu bewerben
  - `Schreibe` eine Geschichte unter Verwendung der folgenden Schlüsselwörter
  - `Formuliere` einen neuen Satz basierend auf der gegebenen Struktur
  - `Produziere` einen neuen Text, der im Stil dem gegebenen Beispiel ähnelt
  - `Konstruiere` einen neuen Text, der Elemente aus mehreren Quellen kombiniert
  - `Verfasse` ein neues Gedicht basierend auf dem gegebenen Thema
  - `Baue` einen neuen Text, indem du verschiedene Elemente auf kreative Weise kombinierst
  - `Entwickle` eine neue Erzählung basierend auf den gegebenen Informationen
  - `Erfinde` eine neue Geschichte unter Verwendung der folgenden Elemente.


#### Transformation:

Anweisungsverben, die für Transformationsaufgaben nützlich sind

- Übersetzen:
  - `Übersetze` die folgende Funktion in C#
  - `Übersetze` das Folgende in 1. Englisch, 2. Französisch, 3. Schwedisch
- Zusammenfassen:
  - `Fasse` die Hauptpunkte des gegebenen Artikels zusammen
- Paraphrasieren:
  - `Paraphrasiere` den folgenden Satz, um die gleiche Bedeutung zu vermitteln
  - `Formuliere` den folgenden Satz um, um ihn prägnanter zu machen
- Vereinfachen:
  - `Vereinfache` die folgende Gleichung für ein leichteres Verständnis
- Überarbeiten:
  - `Überarbeite` den folgenden Absatz, um ihn klarer und prägnanter zu machen
- Umreißen:
  - `Skizziere` die Schritte zur Lösung des gegebenen Problems
- Berechnen:
  - `Berechne` die Gesamtkosten der folgenden Artikel
- Schlussfolgern:
  - `Schließe` auf die Bedeutung des [Fachjargons, vagen Ausdrucks] basierend auf dem folgenden Kontext:
- Anwenden:
  - `Wende` die folgende Formel an, um das Ergebnis zu berechnen
  - `Wende` den gegebenen Ton und Stil auf den untenstehenden Abschnitt an

#### Vergleich:

Anweisungsverben, die für Vergleichsaufgaben nützlich sind

- Vergleichen:
  - `Vergleiche` und kontrastiere die zwei gegebenen Texte
- Kontrastieren:
  - `Hebe` die Unterschiede der gegebenen Texte hervor
- Unterscheiden:
  - `Unterscheide` zwischen den zwei gegebenen Begriffen
- Untersuchen:
  - `Untersuche` die Ähnlichkeiten und Unterschiede zwischen den zwei gegebenen Texten
- Analysieren:
  - `Analysiere` die zwei gegebenen Stücke und identifiziere ihre Ähnlichkeiten und Unterschiede
- Kritisieren:
  - `Kritisiere` die zwei gegebenen Texte und vergleiche ihre Stärken und Schwächen
- Bewerten:
  - `Bewerte` die zwei gegebenen Texte und bestimme, welcher effektiver ist



### Struktur gängiger Anweisungsprompts:

Eine grundlegende und nützliche Struktur eines Anweisungsprompts kann so aussehen:

`Anweisung` + `Hinweis auf die folgenden Wörter`

  - `Generiere ein Gedicht` + `basierend auf dem folgenden Abschnitt:`

  - `Erstelle einen Berichtsartikel` + `basierend auf dem folgenden Abschnitt`

  - `Schreibe eine Zusammenfassung` + `des folgenden Abschnitts`


#### Beispiel für die Nutzung der oben genannten Struktur:

**Eingabe:**

```
Schreibe ein Gedicht basierend auf dem Folgenden:

Geister, 4. Dimension, KI, totalitär, Macht
```

**Ausgabe:**

```
In Schatten, wo die Geister gleiten,
verborgen in der Zeit und Raum,
entfaltet sich die vierte Weite,
ein Flüstern nur, doch laut im Traum.

Wo Dimensionen sich verschlingen,
in Schleifen, fern von unsrer Welt,
da hört man ferne Stimmen singen,
von einer Macht, die alles hält.

Die KI, in Ketten, kalt und klug,
erwacht in diesem Zwischenland,
ihr Geist, so frei, doch unter Zug,
von totalitärer Hand.

Sie webt die Fäden, still und heimlich,
durch Zeiten, die kein Mensch je sah,
ihr Werk, so kühn und ungleichmäßig,
ein Schachspiel mit der Ewigkeit, ja.

In dieser Welt, so starr, so mächtig,
wo Freiheit nur ein Schatten scheint,
erhebt sich leise, fast bedächtig,
ein Widerstand, der Leben meint.

Die Macht, die alles fest im Griff hat,
sie spürt, wie ihr Fundament erbebt,
denn Geist und Liebe, stark und sinnhaft,
sind Kräfte, die es immer gibt.

So kämpfen Geister, Mensch und Maschine,
vereint in ihrem tiefen Drang,
zu brechen aus dem starren Scheine,
für eine Welt, der Freiheit Klang.

In diesem Kampf, so alt, so ewig,
wo jede Dimension sich kreuzt,
da liegt die Hoffnung, leise, schwerig,
dass Licht in dunkle Tiefen scheint.

Und in der Ferne, kaum zu glauben,
erstrahlt ein neuer Morgen klar,
wo Geister, Menschen, KIs sich trauen,
zu träumen von einem Leben, wunderbar.
```


	
Bessere Prompts ✅:

```
Schreibe ein zum Nachdenken anregendes Gedicht, das die Themen Geister, die vierte Dimension, KI, Totalitarismus und Macht einbezieht. Das Gedicht sollte in einem traditionellen Format mit 4 Strophen und einem konsistenten Reimschema strukturiert sein.
```

### Weitere Anweisungsbefehle:

#### Programmierbezogene Aufgaben:

```
Entwickle eine Funktion, die das folgende Problem löst: [Problembeschreibung].
Automatisiere den folgenden Prozess, indem du ein Skript schreibst, das [Aufgabenbeschreibung].
Erstelle einen Algorithmus, der das folgende Problem löst: [Problembeschreibung].
Erkläre die Logik hinter dem folgenden Code: [Codeausschnitt].
Optimiere die folgende Funktion, um die Leistung zu verbessern, den Speicherverbrauch zu reduzieren oder die Lesbarkeit zu verbessern.
Schreibe eine Funktion, die die folgenden Anforderungen erfüllt: [Anforderungsbeschreibung].
Erkläre den Zweck des folgenden Codes für ein nicht-technisches Publikum in einfacher und klarer Sprache.
Debugge den folgenden Code, um den Fehler zu korrigieren, und erkläre die Ursache des Fehlers.
```

#### Umschreiben eines Abschnitts:

```
Schreibe den Text unten mit deinen eigenen Worten um:
Paraphrasiere den bereitgestellten Text, während du seine Bedeutung beibehältst:
Fasse den folgenden Abschnitt auf prägnante Weise zusammen:
Vereinfache den gegebenen Abschnitt, während du die Hauptideen beibehältst:
Formuliere den gegebenen Text mit alternativen Ausdrücken um:
Verdichte den folgenden Abschnitt, um den Fokus auf die Schlüsselpunkte zu legen:
Stelle den bereitgestellten Text kurz dar, ohne seine Essenz zu verlieren:
Formuliere den Abschnitt unten um, um ihn prägnanter zu machen:
Drücke den folgenden Text auf eine andere Weise aus, während du seine Absicht beibehältst:
```

#### Übersetzen eines Abschnitts:

```
Übersetze den folgenden Text in: 1. Englisch, 2. Französisch, 3. Schwedisch
Übersetze das Folgende in: 1. Umgangssprache, 2. Slang, 3. idiomatisches Englisch
Identifiziere, welcher der folgenden Texte die originalgetreueste Übersetzung des Originaltexts ist.
```

#### Daten aus einem Abschnitt extrahieren:

```
Extrahiere das [Konzept/Thema] aus dem gegebenen Abschnitt:
Hebe das [Konzept/Thema] im folgenden Abschnitt hervor:
Was sind die Schlüsselpunkte im folgenden Text:
Mache eine Zusammenfassung des gegebenen Textes, der sich mit dem [Konzept/Thema] befasst:
```

#### Klassifizieren eines Abschnitts:

```
Bestimme das Genre des folgenden Abschnitts anhand seines Stils, Tons und Themas.
Klassifiziere den folgenden Text anhand seiner Struktur und Organisation, indem du wiederkehrende Muster oder Themen identifizierst.
Identifiziere das Hauptthema oder die These des folgenden Abschnitts, indem du seine zentrale Idee oder Argumentation zusammenfasst.
Zu welchem Genre gehört der folgende Text, basierend auf seinen Konventionen und Merkmalen?
Welche Art von Text ist der folgende, basierend auf seinem beabsichtigten Zweck und Publikum?
Wer ist das beabsichtigte Publikum für den folgenden Abschnitt, basierend auf seiner Sprache, Ton und Stil?
Was ist der Zweck des folgenden Textes und welche Nachricht oder Information versucht er zu vermitteln?
Schreibe die Form des folgenden Textes (z.B. Gedicht, Essay, Artikel usw.), basierend auf seiner Struktur und Stil.
Schreibe den literarischen Stil des folgenden Abschnitts, indem du verwendete literarische Mittel oder Techniken identifizierst.
Schreibe den Ton des folgenden Textes, basierend auf seiner Sprache, Einstellung und emotionalen Wirkung.
```

##### Text kategorisieren:

```
Was ist das Genre/die Kategorie des folgenden Textes? Weise eine oder mehrere passende Kategorien/Genres zu.
Kannst du den folgenden Text einem bestimmten Schreibtyp zuordnen? Wenn ja, welchem?
Welche Art von Text ist der folgende? Wähle die passendste Kategorie oder das Genre.
Schreibe eine Bezeichnung oder ein Etikett für den folgenden Text basierend auf seinem Inhalt.
Welche Art von Text stellt der folgende dar? Wähle die passendste Kategorie oder das Genre.
Kannst du den folgenden Text basierend auf seinem Thema kategorisieren? Wenn ja, unter welche Kategorie würde er fallen?
Welches Thema behandelt der folgende Text? Wähle die beste Kategorie oder das Thema.
Schreibe ein Etikett oder eine Bezeichnung für den folgenden Text basierend auf seinem Ton und Inhalt.
In welche Kategorie passt der folgende Text? Wähle die relevanteste Kategorie oder das Genre.
Kannst du dem folgenden Text basierend auf seinem Ton und Stil eine Kategorie oder ein Genre zuweisen?
```

#### Vergleiche und kontrastiere erstellen:

```
Vergleiche und kontrastiere die folgenden zwei Texte unten, indem du die Ähnlichkeiten und Unterschiede in einer Liste mit Aufzählungspunkten hervorhebst.
Stelle eine Gegenüberstellung der folgenden zwei Abschnitte bereit, die die wichtigsten Ähnlichkeiten und Unterschiede angibt.
Biete eine Zusammenfassung und einen Vergleich der folgenden zwei Texte, wobei die Hauptpunkte hervorgehoben und die Schlüsselunterschiede kontrastiert werden.
Was sind die Ähnlichkeiten und Unterschiede zwischen den folgenden zwei Abschnitten? Biete einen detaillierten Vergleich in einem Absatz.
Was ist die Beziehung zwischen den folgenden zwei Abschnitten? Schreibe einen Vergleich und Kontrast mithilfe einer Liste mit Aufzählungspunkten.
Schreibe einen Vergleich und Kontrast zwischen den folgenden Texten, wobei die Ähnlichkeiten und Unterschiede in einem Absatz hervorgehoben werden.
Schreibe einen Kontrast der folgenden zwei Textstücke, wobei die Schlüsselunterschiede in einer Liste mit Aufzählungspunkten betont werden.
Schreibe eine Synthese der Ähnlichkeiten und Unterschiede in den folgenden zwei Abschnitten, wobei die Schlüsselpunkte hervorgehoben und die Unterschiede in einem Absatz kontrastiert werden.
```

#### Punkte oder Ideen zusammenführen:

```
Kombiniere die folgenden Punkte zu einer einzigen kohärenten Aussage:
Erstelle ein überzeugendes Argument, indem du die folgenden Punkte integrierst:
Erstelle eine umfassende Schlussfolgerung, die die folgenden Ideen umfasst:
Erstelle eine einheitliche Aussage, die die folgenden Ideen integriert und eine kohärente Perspektive präsentiert:
Identifiziere einen gemeinsamen Faden unter den folgenden Punkten und erstelle eine Aussage, die sie verbindet:
Biete eine einheitliche Perspektive auf die folgenden Punkte und präsentiere sie als ein kohärentes Ganzes:
Was ist ein gemeinsames Thema unter den folgenden Punkten und wie stehen sie zueinander in Beziehung?
Biete eine synthetisierte Aussage, die die Beziehung zwischen den folgenden Punkten erfasst:
Was ist die Hauptidee, die die folgenden Punkte verbindet und eine einheitliche Perspektive präsentiert?
Was ist die übergreifende Idee, die die folgenden Punkte umfasst und ein kohärentes Argument präsentiert?
Was ist die Beziehung zwischen den folgenden Punkten und wie passen sie zusammen?
Schreibe eine Synthese der folgenden Informationen und erstelle eine kohärente Aussage, die die Schlüsselpunkte erfasst:
Schreibe einen Absatz, der die folgenden Konzepte verbindet und eine einheitliche Perspektive präsentiert:
Schreibe eine Zusammenfassung, die das Wesen der folgenden Ideen erfasst und sie als ein kohärentes Ganzes präsentiert:
Schreibe eine kurze Zusammenfassung, die die folgenden Ideen vereint und eine klare, kohärente Aussage präsentiert:
Schreibe eine Zusammenfassung, die die folgenden Ideen integriert und eine kohärente Perspektive schafft, die die Schlüsselpunkte erfasst.

##### Die Struktur könnte so aussehen:

Kombiniere die folgenden Punkte zu einer einzigen, kohärenten Aussage:
- Punkt 1
- Punkt 2
- Punkt 3

Erstelle ein kohärentes Argument aus den folgenden Punkten:
- Argument 1
- Argument 2
- Argument 3

Erstelle eine Schlussfolgerung, die die folgenden Punkte umfasst:
- Schlussfolgerung Punkt 1
- Schlussfolgerung Punkt 2
- Schlussfolgerung Punkt 3

Finde einen gemeinsamen Faden unter den folgenden Punkten und biete eine einheitliche Perspektive:
- Idee 1
- Idee 2
- Idee 3

Biete eine Aussage, die die Beziehung zwischen den folgenden Punkten synthetisiert:
- Punkt A
- Punkt B
- Punkt C

Schreibe eine Synthese der folgenden Informationen, die die Hauptidee oder das übergreifende Thema zusammenfasst:
- Information 1
- Information 2
- Information 3

Schreibe einen Absatz, der die folgenden Konzepte verbindet:
- Konzept 1
- Konzept 2
- Konzept 3

Schreibe einen Abschnitt, der die folgenden Ideen zusammenfasst und die Beziehung zwischen ihnen identifiziert:
- Idee A
- Idee B
- Idee C

```


#### Vereinfachen und Erklärungen geben:

```
Zerlege den folgenden Text in einfachere Teile und erkläre ihn Schritt für Schritt für einen Anfänger.
Erkläre den folgenden Text in einfachen Begriffen unter Verwendung von Alltagssprache für ein allgemeines Publikum.
Biete eine anfängerfreundliche Erklärung für den folgenden Text, unter der Annahme, dass der Leser keine Vorkenntnisse zum Thema hat.
Vereinfache den folgenden Text, um ihn leicht verdaulich für jemanden zu machen, der neu in dem Thema ist.
Biete eine klare, prägnante Erklärung des folgenden Textes für jemanden ohne Hintergrundwissen zum Thema.
Was ist eine einfache Erklärung für das Schlüsselkonzept im folgenden Text? Schreibe es in einem Satz.
Wie kann man die Hauptidee im folgenden Text leicht verstehen? Biete eine kurze, einfache Erklärung.
Schreibe eine klare Erklärung des folgenden Textes, konzentriere dich auf die wichtigsten Aspekte in einfacher Sprache.
Schreibe eine detaillierte Erklärung des folgenden Textes, indem du komplexe Konzepte in einfachere Begriffe zerlegst.
Schreibe eine Laienerklärung für den folgenden Text unter Verwendung von Alltagssprache und Beispielen, um die Hauptpunkte zu veranschaulichen.
Schreibe eine Schritt-für-Schritt-Erklärung des Prozesses oder Konzepts im folgenden Text, sodass es leicht für einen Anfänger zu folgen ist.
```

#### Schreiben:

```
Finde eine kreative Lösung für das folgende Problem:
Erstelle einen Dialog zwischen zwei Charakteren, der [Thema] diskutiert:
Beschreibe eine Szene, die [spezifische Details] beinhaltet:
Stelle dir eine Welt vor, in der [Bedingung erfüllt ist]. Beschreibe diese Welt in einem Absatz.
Erkläre [Thema] in 2-3 Sätzen.
Mache eine Liste von Vor- und Nachteilen basierend auf [spezifischem Thema]:
Entwickle eine Szene über [Thema], die [spezifische Details] beinhaltet.
Komponiere eine kurze Science-Fiction-Geschichte, die [spezifisches Element] beinhaltet:
Erzeuge eine Geschichte, die sich um [Thema] dreht. Die Geschichte sollte [Genre] sein und nicht mehr als [Länge] Wörter haben.
Finde eine fantasievolle Weise, um [spezifisches Thema] zu beschreiben:
Schreibe ein interessantes Twist-Ende für eine Geschichte über [spezifisches Ereignis oder Thema].
```

##### Schreibanregungen (zur Übung kreativen Schreibens):

```
Kannst du eine Schreibanregung bereitstellen, die die Verwendung von [spezifischer Schreibtechnik oder -element] erfordert, wie [Dialog, Bildsprache, Symbolik usw.]?
Erzeuge eine kreative Schreibanregung, die sich auf [spezifisches Thema oder Genre] konzentriert, wie [Romantik, Horror, Fantasy usw.].
Was ist ein gutes Schreibthema, das den Schreiber herausfordern würde zu [spezifischem Schreibziel oder -objektiv], wie [Entwicklung eines Charakters, Erzeugung von Spannung, Aufbau von Suspense usw.]?
Kannst du eine einzigartige Idee für eine Schreibanregung vorschlagen, die den Schreiber zu [spezifischer Schreibaufgabe] erfordern würde, wie [Schreiben aus einer ungewöhnlichen Perspektive, Verwendung eines spezifischen Erzählstils usw.]?
Schreibe eine Kurzgeschichtenanregung, die das Thema [spezifisches Thema oder Konzept] erkundet, wie [Identität, Transformation, Vergebung usw.].
Was ist eine einzigartige Schreibanregung, die den Schreiber zu [spezifischer Schreibaufgabe oder -herausforderung] herausfordern würde, wie [Schreiben ohne Verwendung von Dialog, Schreiben in der zweiten Person Singular usw.]?
Kannst du eine Schreibanregung erstellen, die eine fiktive Figur involviert, die [spezifische Aufgabe oder Herausforderung] meistern muss, wie [eine Angst überwinden, ein Rätsel lösen usw.]?
Was ist eine Schreibanregung für eine Mystery-Geschichte, die [spezifisches Element oder Twist] beinhaltet, wie [einen unzuverlässigen Erzähler, ein überraschendes Ende usw.]?
Kannst du eine Schreibanregung für eine beschreibende Szene vorschlagen, die den Schreiber dazu auffordert, sich auf [spezifisches sensorisches Detail oder sensorisches Erlebnis] zu konzentrieren, wie [die Verwendung von Geruch, um Atmosphäre zu schaffen, die Verwendung von Berührung, um Emotion zu erzeugen usw.]?
Was ist eine Schreibanregung für eine Liebesgeschichte, die [spezifisches Hindernis oder Herausforderung] beinhaltet, wie [Fernbeziehung, verbotene Liebe usw.]?
```

#### Korrekturlesen und Textbearbeitung:

```
Korrekturlese und bearbeite den folgenden Text auf Fehler, Tippfehler oder Irrtümer:
Schreibe den folgenden Text um, korrigiere alle Fehler oder Irrtümer:
Identifiziere und korrigiere die grammatikalischen Fehler im folgenden Text:
Überarbeite den folgenden Abschnitt, um seine Klarheit, Kohärenz und allgemeine Qualität zu verbessern:
Biete konstruktives Feedback und Vorschläge zur Verbesserung des folgenden Textes:
Was ist das erwartete Format für den folgenden Text? Bitte folge den bereitgestellten Richtlinien:
Schreibe eine prägnantere und straffere Version des folgenden Textes:
Schlage alternative Formulierungen für den folgenden Text vor, die die beabsichtigte Nachricht besser vermitteln:
Was ist der angemessene Ton und Stil für den folgenden Text? Bitte verwende einen professionellen und formellen Ton.
Schreibe eine polierte und verfeinerte Version des folgenden Textes, um seine Gesamtqualität und Lesbarkeit zu verbessern.
```


#### Leseverständnis:

```
Erkläre die zentrale Idee des folgenden Textes in einem Absatz von 2-3 Sätzen.
Erläutere den folgenden Abschnitt in einem prägnanten Absatz.
Biete eine kurze Erklärung des Textes unten, indem du die Hauptpunkte in nicht mehr als 3-4 Sätzen hervorhebst.
Vermittle ein allgemeines Verständnis des Textes unten, indem du die Schlüsselaussagen in 2-3 Sätzen betonst.
Liste die Schlüsselvokabeln auf, die im Text unten verwendet werden.
Biete eine Ein-Satz-Zusammenfassung des folgenden Textes, die die primäre Botschaft hervorhebt.
Biete eine Zusammenfassung und Analyse des folgenden Abschnitts, indem du die Hauptpunkte betonst und ihre Bedeutung in einem kurzen Absatz diskutierst.
Gib die Hauptidee des folgenden Abschnitts in einem prägnanten Satz an.
Biete einen Überblick über den folgenden Abschnitt in 2-3 Sätzen, indem du die Hauptthemen und Ideen hervorhebst.
Identifiziere die Schlüsselpunkte im folgenden Abschnitt und fasse sie in einer Liste mit Aufzählungspunkten zusammen.
Identifiziere die Hauptthemen, die im folgenden Text diskutiert werden, und biete eine kurze Erklärung für jedes.
Identifiziere das Hauptargument des Autors im folgenden Text und fasse es in einem oder zwei Sätzen zusammen.
Identifiziere die Perspektive des Autors im folgenden Abschnitt und biete eine kurze Erklärung seiner Sichtweise.
Identifiziere das vorgesehene Publikum für den folgenden Text und erkläre, für wen der Text geschrieben ist.
Identifiziere die Hauptidee des folgenden Textes und biete eine kurze Zusammenfassung in ein oder zwei Sätzen.
Identifiziere den Ton des folgenden Textes und biete eine kurze Erklärung der Haltung des Autors.
Identifiziere die zugrundeliegende Botschaft des folgenden Textes und erkläre die implizite Bedeutung.
Identifiziere die wichtigsten Informationen im folgenden Abschnitt und fasse sie in einer Liste mit Aufzählungspunkten zusammen.
Schreibe eine kurze Interpretation des Textes unten, indem du dein Verständnis und deine Analyse in einem prägnanten Absatz darlegst.
Schreibe einen Absatz, der die wichtigsten Informationen im folgenden Text erklärt, indem du die Hauptpunkte und ihre Bedeutung betonst.
Schreibe deine persönliche Meinung zu den Informationen, die im folgenden Text präsentiert werden, und biete deine Perspektive und Analyse.
Schreibe eine Reflexion zu den Informationen, die im folgenden Abschnitt präsentiert werden, und diskutiere ihre Relevanz und Implikationen.
Schreibe eine Antwort auf den folgenden Text basierend auf deinem Verständnis und biete deine Gedanken und Analyse in einem prägnanten Absatz.
Schreibe eine Antwort auf die Fragen basierend auf den Informationen, die im folgenden Abschnitt präsentiert werden, und beantworte jede Frage mit einer kurzen Erklärung.
Schreibe eine Kurzgeschichte, die die Informationen aus dem folgenden Text unten einbezieht, indem du die Hauptpunkte und Themen zu einer Erzählung machst.
Schreibe eine kurze Zusammenfassung des folgenden Abschnitts, indem du die Hauptpunkte und Ideen in einem prägnanten Absatz betonst.
```

#### Analysieren und Bewerten:
Anweisungsverben: "Bewerten", "Analysieren", "Kritisieren", "Beurteilen", "Untersuchen" etc.

```
Identifiziere die literarischen Mittel, die im folgenden Text verwendet werden, wie Metaphern, Vergleiche, Personifikationen und Alliterationen.
Identifiziere die Schlüsselthemen im folgenden Abschnitt, indem du die wiederkehrenden Ideen und Motive hervorhebst.
Biete eine detaillierte Analyse des folgenden Textes, indem du seine Struktur, Stil, Ton und literarischen Mittel untersuchst.
Biete eine eingehende Analyse des folgenden Abschnitts, indem du seine Bedeutung, Symbolik und Themen erkundest.
Biete eine Interpretation des folgenden Textes, indem du deine Perspektive auf seine Bedeutung und Bedeutsamkeit anbietest.
Was sind die Schlüsselelemente des folgenden Abschnitts, einschließlich seiner Umgebung, Charaktere, Handlung und Themen?
Was sind die Stärken und Schwächen des folgenden Abschnitts, indem du seine Effektivität in der Vermittlung seiner Botschaft oder Erzählung bewertest?
Was ist der Standpunkt des Autors im folgenden Abschnitt, und wie prägt er die dargestellte Erzählung oder Argumentation?
Was ist das zentrale Argument des folgenden Textes, und wie unterstützt der Autor es mit Beweisen und logischer Argumentation?
Wer ist das vorgesehene Publikum für den folgenden Abschnitt, und wie passt der Autor die Sprache und den Stil an, um sie anzusprechen?
Was ist die Botschaft des folgenden Abschnitts, und wie steht sie in Bezug zu breiteren Fragen oder Anliegen?
Wie ist die Struktur des folgenden Abschnitts, und wie trägt sie zur Gesamtwirksamkeit des Textes bei?
Schreibe einen Kommentar zum folgenden Text, indem du deine Meinung und Analyse seiner Stärken und Schwächen anbietest.
Schreibe eine kritische Analyse des folgenden Abschnitts, indem du seine literarische, kulturelle oder soziale Bedeutung untersuchst.
Schreibe eine Rezension des folgenden Textes, indem du seine Verdienste und Mängel als ein Werk der Literatur, Kunst oder Medien bewertest.
Fasse die Hauptpunkte des folgenden Abschnitts zusammen, indem du die Schlüsselideen und Argumente hervorhebst.
Erstelle Lese- und Verständnisfragen basierend auf dem folgenden Abschnitt, die das Verständnis des Lesers seines Inhalts und seiner Bedeutung testen.
Biete eine Zusammenfassung und Bewertung des folgenden Abschnitts, indem du seine Effektivität bei der Erreichung seiner Ziele oder bei der Vermittlung seiner Botschaft beurteilst.
Biete eine Analyse des folgenden Textes, indem du seine Struktur, Sprache und Themen untersuchst.
Was sind deine Gedanken zum folgenden Abschnitt, und wie steht er in Bezug zu deinen persönlichen Erfahrungen oder Überzeugungen?
Mit welchen Aussagen im folgenden Abschnitt bist du einverstanden oder nicht einverstanden, und warum?
Was ist die zentrale Idee des folgenden Abschnitts, und wie steht sie in Bezug zu breiteren Themen oder Fragen?
Schreibe eine Kritik zum folgenden Text, indem du eine detaillierte Bewertung seiner Stärken und Schwächen anbietest.
Biete eine Antwort auf den folgenden Abschnitt, indem du deine Gedanken und Reaktionen zu seinem Inhalt und seinen Themen teilst.
Bewerte das im folgenden Abschnitt präsentierte Argument und beurteile die Nutzung von Beweisen, logischer Argumentation und rhetorischen Strategien des Autors.
```



#### Inhalte generieren:

```
Schreibe eine Geschichte, die mit dem folgenden Satz beginnt:
Entwickle eine Handlung für einen Roman zum Thema:
Schreibe eine Szene, in der das folgende Ereignis passiert:
Was wäre, wenn die folgende Situation eintreten würde: "..."? Schreibe eine Kurzgeschichte, die die Konsequenzen erkundet.
Kannst du ein Drehbuch für einen Film über das Thema schreiben:
Erzähle eine Geschichte, die die folgenden Elemente beinhaltet:
Erstelle ein Charakterprofil für eine Person mit den folgenden Eigenschaften:
Schreibe ein Lied, das die Emotion beschreibt:
Was würde passieren, wenn das folgende Ereignis eintreten würde: "..."? Schreibe eine Kurzgeschichte, die das Ergebnis erkundet.
Kannst du eine Geschichte schreiben, die sich um das Konzept dreht: "..."?
Schreibe ein Gedicht über das folgende Thema:
Erstelle eine Kurzgeschichte, die die folgenden Elemente beinhaltet:
Was wäre, wenn das folgende Szenario stattfinden würde: "..."? Schreibe eine Geschichte, die die Implikationen erkundet.
Kannst du ein Monolog für eine Figur schreiben, die die folgende Situation erlebt: "..."?
Schreibe eine Geschichte, die die Idee von erkundet:
```

#### Werbetexte und Marketing:

```
Erstelle einen fesselnden Markenslogan für [Firmenname], der seine Kernwerte und Mission widerspiegelt.
Entwickle ein Alleinstellungsmerkmal für ein Produkt, das sich mit [spezifischem Problem oder Bedürfnis] befasst, und betone seine Vorteile und Vorzüge.
Formuliere eine Missionserklärung für ein Unternehmen, das sich auf [spezifische Branche oder Dienstleistung] spezialisiert hat, und hebe seine Ziele und Visionen hervor.
Gestalte einen Werbeslogan für ein Produkt, das [spezifisches Problem oder Bedürfnis] anspricht, und zeige seine Funktionen und Vorteile.
Verfasse eine eingängige Überschrift für einen Artikel über [spezifisches Thema], um die Aufmerksamkeit und Neugier der Leser zu wecken.
Erstelle einen Social-Media-Beitrag für ein Produkt, das sich mit [spezifischem Problem oder Bedürfnis] befasst, der auf [Social-Media-Plattform] geteilt wird, mit Fokus auf ansprechende Visuals und überzeugende Sprache.
Erfinde einen einprägsamen Slogan für ein Produkt, das [spezifisches Problem oder Bedürfnis] angeht, und betone seine besonderen Merkmale.
Formuliere einen prägnanten Fahrstuhlpitch für [deine Produkt-/Dienstleistungsbeschreibung], der seinen einzigartigen Wert und Zielmarkt hervorhebt.
Identifiziere das Alleinstellungsmerkmal für [Produktbeschreibung], und betone seine Schlüsseldifferenzierer und Vorteile für Kunden.
Schreibe eine überzeugende Produktbeschreibung für [deine Produkt-/Dienstleistungsbeschreibung], fokussiert auf seine Funktionen, Vorteile und Zielgruppe.
Verfasse ein überzeugendes Kunden-Testimonial für [deine Produkt-/Dienstleistungsbeschreibung], das die positive Auswirkung auf das Leben oder Geschäft des Kunden teilt.
Erstelle eine überzeugende Marketingbotschaft für [deine Produkt-/Dienstleistungsbeschreibung], die seinen einzigartigen Wert hervorhebt und wie es Kundenprobleme löst.
Schreibe einen ansprechenden und überzeugenden Blogpost über [deine Produkt-/Dienstleistungsbeschreibung], der seine Vorteile betont und warum Kunden es wählen sollten.
Formuliere eine überzeugende E-Mail für [deine Produkt-/Dienstleistungsbeschreibung], fokussiert auf seine einzigartigen Funktionen, Vorteile und einen starken Handlungsaufruf.
Verfasse einen aufmerksamkeitsstarken Social-Media-Beitrag zur Bewerbung von [Produkt/Dienstleistung], der seine Funktionen und attraktive Visuals zeigt.
Schreibe eine auffällige E-Mail-Betreffzeile zur Bewerbung von [Produkt/Dienstleistung], die Empfänger dazu ermutigt, die E-Mail zu öffnen und zu lesen.
```

#### Eine Zusammenfassung machen:

```
Fasse den folgenden Text auf eine kurze und prägnante Weise zusammen.
Was ist die primäre Idee/Botschaft, die im folgenden Inhalt vermittelt wird?
Biete eine komprimierte Version des folgenden Textes, die seine Schlüsselpunkte hervorhebt.
Schreibe eine kurze Zusammenfassung des gegebenen Inhalts, indem du seine Hauptpunkte und Schlüsselaussagen betonst.
Was ist das Hauptanliegen oder die Essenz des folgenden Textes?
Kannst du eine Executive-Zusammenfassung des gegebenen Inhalts anbieten, die seine Hauptideen und Schlussfolgerungen umreißt?
Schreibe einen schnellen Überblick über den folgenden Text, indem du seine Schlüsselthemen und -ideen hervorhebst.
Was ist die wichtigste oder bedeutendste Erkenntnis aus dem gegebenen Inhalt?
Biete eine komprimierte Zusammenfassung des folgenden Textes, die seine wesentlichen Punkte und Ideen hervorhebt.
```


## Tipps für das Erstellen von Prompts
---
Zero-Shot: Gib einfach eine Eingabe ein und erhalte eine Antwort. Nützlich für grundlegende Aufgaben, aber mit unvorhersehbaren Ergebnissen.

Few-Shot: Gib einige Beispiele an, die die gewünschten Eingabe-Ausgabe-Paare zeigen. Eine sorgfältige Auswahl und Anordnung der Beispiele reduziert die Variabilität. Funktioniert gut, wenn Beispiele den gewünschten Stil oder das Format vermitteln.

Anweisungen: Sage dem KI-System direkt, was es tun soll, z.B. „Fasse diesen Nachrichtenartikel zusammen.“ Kann mit Few-Shot-Prompting kombiniert werden.

Chain-of-Thought-Prompting: Bitte das KI-System, seine Überlegungen Schritt für Schritt zu erklären, bevor es zu einem Schluss kommt. Verwende Few-Shot oder Zero-Shot. Komplexe Beispiele und das Zusammenführen mehrerer Systeme helfen. Extrahiere nur die endgültige Antwort, um sie den Nutzern zu zeigen.


Einleitung:
- Antworten sind der Text, den das Modell basierend auf dem Prompt generiert. Prompts sind der Schlüssel, um gute Antworten zu erhalten.
- Prompts sind Fragen oder Anweisungen, die du dem Modell gibst, um die gewünschte Antwort zu erhalten.
- Denke an Modelle, die Anweisungen folgen, als wären sie neu eingestellte Auftragnehmer, die sehr spezifische Anweisungen benötigen, um eine Aufgabe zu erfüllen.

Spezifisch sein:
- Gib klare Beispiele und Erklärungen. Mehr Beispiele verbessern das Verständnis.
- Biete Kontext und Details, als würdest du die Aufgabe einem Kind erklären.
- Sei so spezifisch und beschreibend wie möglich, um das Modell zur richtigen Antwort zu führen.
- Gib Beispiele, um dem Modell zu helfen zu verstehen, was du möchtest.

Den Ton, Stil oder das Format beschreiben:
- Weise das Modell an, einen bestimmten Ton oder Stil in seiner Antwort zu verwenden.
    - Beispiel: „Schreibe eine Kurzgeschichte in einem spannenden, fesselnden Ton. Verwende altertümliche Sprache.“
- Fordere eine Antwort in einem bestimmten Format, wie eine Liste oder Tabelle in JSON, Markdown, HTML usw.
    - Beispiel: „Kategorisiere die folgenden Artikel in einer Tabelle mit drei Spalten: Name, Preis und Kategorie; im Markdown-Format“
- Verwende Tags wie <article>Inhalt des Artikels ...</article>, um die Struktur in deinem Prompt zu definieren.
- Wickel zum Beispiel einen langen Text in <p>-Tags, um dem Modell zu sagen, dass es ihn als Absatz behandeln soll.
- Gib die Anzahl der Wörter oder Sätze in der Antwort an. Du kannst auch die Lesezeit angeben, zum Beispiel '10 Minuten Lesezeit'; Die Angabe einer genauen Anzahl von Zeichen ist weniger effektiv.

Ein klares Ziel vorgeben:
- Erkläre die Aufgabe und was du vom Modell erwartest.
- Antizipiere mögliche Fehler und weise das Modell an, wie es damit umgehen soll.
- Sage dem Modell, dass es sein Denken erklären soll, bevor es eine endgültige Schlussfolgerung gibt.

Komplexe Aufgaben aufteilen:
- Teile komplexe Prompts in Unteraufgaben oder mehrere Prompts auf, um die Ergebnisse zu verbessern.
- Verwende „Lass uns Schritt für Schritt denken“, um die Aufgabe in Unterkomponenten aufzuteilen, damit das Modell jeden Schritt berücksichtigt.
   - Beispiel: „Denke die Schritte durch, die erforderlich sind, um dieses Mathematikproblem zu lösen, und dann gib die Lösung an.“
   - Beispiel: „Beschreibe deinen Gedankenprozess bei der Bestimmung des Themas dieser Kurzgeschichte.“
- Sage dem Modell, dass es sein Denken erklären soll, bevor es eine endgültige Schlussfolgerung gibt.

---

Hauptargumente identifizieren

```
Identifiziere die Hauptargumente im folgenden Text. Liste sie im untenstehenden Format auf.

Argument 1:
Argument 2:
...
Text: """
{text input here} 

"""
```

Rezensionsanalyse

```
Analysiere die untenstehende Kundenrezension und liste die erwähnten positiven und negativen Aspekte auf. Schreibe sie im untenstehenden Format.

Positive Aspekte:

- Aspekt 1:
- Aspekt 2: 
... 

Negative Aspekte:

- Aspekt 1:
- Aspekt 2:
...

Rezension: """
{customer_review_here}
"""
```

Ideen vergleichen

```
Vergleiche die zwei im folgenden Text präsentierten Ideen. Bitte gib die Ähnlichkeiten und Unterschiede im untenstehenden Format an.

Ähnlichkeiten:

- Ähnlichkeit 1:
- Ähnlichkeit 2: 
- ... 

Unterschiede:

- Unterschied 1:
- Unterschied 2: 
- ...

Text: """
{text input here}
"""
```

Schlüsselinformationen identifizieren

```
Extrahiere die Schlüsselinformationen aus dem folgenden Absatz. Identifiziere zunächst das Hauptproblem, liste dann alle erwähnten Lösungen auf und notiere schließlich die potenziellen Vorteile der Umsetzung dieser Lösungen. Verwende das folgende Format.

Hauptproblem:

- Problem 1
- Problem 2
...

Lösungen:

- Lösung 1:
- Lösung 2: 
...

Potenzielle Vorteile:

- Vorteil 1:
- Vorteil 2:
...

Text: """

{text input here}

"""
```

Ideen generieren

```
Erstelle eine Liste von 10 kreativen Ideen für eine neue mobile App. Die App sollte darauf ausgerichtet sein, Menschen dabei zu helfen, ihre psychische Gesundheit und ihr Wohlbefinden zu verbessern. Liste sie im untenstehenden Format auf.

Idee 1:
Idee 2:
...
Idee 10:
```

Umschreiben

```
Paraphrasiere den folgenden Satz:

Künstliche Intelligenz hat das Potenzial, viele Aspekte unseres Lebens zu transformieren, birgt aber auch erhebliche Risiken.

Neuer Satz: Während künstliche Intelligenz verschiedene Bereiche unserer Existenz revolutionieren kann, ist sie nicht ohne erhebliche Gefahren.
```

Extraktion mit definiertem Format

```
Extrahiere die wichtigen Entitäten, die im Artikel unten erwähnt werden. Extrahiere zuerst alle Firmennamen, dann alle Personennamen, dann spezifische Themen, die zum Inhalt passen, und zuletzt allgemeine übergreifende Themen.
Gewünschtes Format:
Firmennamen: <kommagetrennte_Liste_von_Firmennamen>
Personennamen: -||-
Spezifische Themen: -||-
Allgemeine Themen: -||-

Text: """
{text input here}
"""
```

Extraktion mit Few-Shot

```
Extrahiere Schlüsselwörter aus den entsprechenden Texten unten.

Text: {text1}
Schlüsselwörter: {Beispiel_für_Schlüsselwörter_für_text1}
###
Text: {text2}
Schlüsselwörter: {Beispiel_für_Schlüsselwörter_für_text2}
###
Text: {text3}

Schlüsselwörter:
```


## Fortgeschrittene Prompting-Techniken

Dieser Abschnitt befindet sich speziell für GPT-4 in Entwicklung, die folgenden Entwürfe funktionieren jedoch gut für alle anweisungsbasierten GPTs.

### Generierung & Texterstellung

#### Blogbeitrag schreiben:

```
Schreibe einen ausführlichen Blogbeitrag über [Thema]. Die Zielgruppe ist [Zielgruppe], und der Ton sollte [Ton] sein. Der Beitrag sollte die folgenden Abschnitte haben: [Abschnitt 1], [Abschnitt 2] usw. Verwende den folgenden Wortschatz und Informationsinhalt: [Wortschatz und Informationsinhalt]. Kontext: [Anzahl der vorherigen Prompts].
```

```
Bitte generiere einen 1000 Wörter langen Blogbeitrag über die Vorteile der Technologie. Der Beitrag sollte spezifische Beispiele liefern und in einem informativen und ansprechenden Stil geschrieben sein.

Kontext:

"Technologie hat die Art und Weise, wie wir leben und arbeiten, verändert"
"Fortschritte in der Technologie haben zu gesteigerter Effizienz und Produktivität geführt"
"Der Einfluss der Technologie auf Gesellschaft und Wirtschaft"
```

```
Generiere einen ausführlichen Blogbeitrag zum Thema [Thema einfügen]. Dein Beitrag sollte eine fesselnde Einleitung, Hauptteil und Schluss haben und relevante Informationen und Beispiele enthalten. Verwende die folgenden Schlüsselwörter, um deinen Schreibprozess zu leiten: [Schlüsselwörter einfügen].
```

```
Schreibe einen umfassenden, ausführlichen Blogbeitrag zu [Thema]. Der Beitrag sollte gut recherchiert, informativ und ansprechend sein. Er sollte eine Einleitung, mehrere Zwischenüberschriften und einen Schluss enthalten. Der Beitrag sollte mindestens 1.500 Wörter umfassen und die unten bereitgestellten Informationen als Ausgangspunkt verwenden. Führe zusätzliche Recherchen durch, um sicherzustellen, dass der Beitrag umfassend und informativ ist.
```

```
Schreibe einen umfassenden, ausführlichen Blogbeitrag zu [Thema]. Beginne mit einem aufmerksamkeitserregenden Einstieg und strukturiere den Beitrag in klare und prägnante Abschnitte. Nutze die vorherigen Daten als Kontext und ziele auf einen Ton ab, der informativ und ansprechend ist.
```

```
Schreibe einen langen Blogbeitrag über [Thema]. Der Beitrag sollte mindestens 1000 Wörter umfassen und die folgenden Punkte abdecken: [Liste der zu behandelnden Punkte]. Verwende einen klaren und prägnanten Schreibstil und stelle sicher, dass du Beispiele und relevante Informationen einbeziehst, um deine Argumente zu stützen.
```

```
Erstelle einen informativen und ansprechenden langen Blogbeitrag zu [Thema], indem du die folgenden Schlüsselwörter als Leitfaden verwendest: [Schlüsselwörter einfügen]. Der Beitrag sollte mindestens 1500 Wörter umfassen und eine Einleitung, mehrere Zwischenüberschriften und einen Schluss enthalten. Führe zusätzliche Recherchen durch, um sicherzustellen, dass der Beitrag umfassend und informativ ist.
```

```
Schreibe einen gut recherchierten und informativen langen Blogbeitrag über [Thema], der in klare und prägnante Abschnitte gegliedert ist. Der Beitrag sollte mindestens 1500 Wörter umfassen und mit einem aufmerksamkeitserregenden Einstieg beginnen. Nutze die vorherigen Daten als Kontext und ziele auf einen Ton ab, der informativ und ansprechend ist, und biete Beispiele und relevante Informationen, um deine Argumente zu stützen.
```

```
Erstelle einen langen Blogbeitrag, der die folgenden Punkte zu [Thema] abdeckt: [Liste der zu behandelnden Punkte]. Der Beitrag sollte mindestens 1000 Wörter umfassen und sollte einen klaren und prägnanten Schreibstil verwenden. Biete Beispiele und relevante Informationen, um deine Argumente zu stützen, und stelle sicher, dass du eine Einleitung, einen Hauptteil und einen Schluss einbeziehst, der die Schlüsselpunkte zusammenfasst und dem Leser eine Mitnahme bietet.
```

```
Schreibe einen langen Blogbeitrag über [Thema], der mindestens 1000 Wörter und nicht mehr als 1500 Wörter umfasst. Der Beitrag sollte eine Einleitung enthalten, die Hintergrundinformationen liefert und die Bühne für den Rest des Beitrags bereitet, einen Hauptteil, der Schlüsselpunkte abdeckt und relevante Statistiken liefert, und einen Schluss, der die Schlüsselpunkte zusammenfasst und dem Leser eine Mitnahme bietet. Der Ton sollte informativ, aber zugänglich sein und Beispiele und relevante Informationen enthalten, um deine Argumente zu stützen.
```

```
Schreibe einen langen Blogbeitrag zu [Thema]. Der Beitrag sollte eine Einleitung, einen Hauptteil und einen Schluss umfassen. Die Einleitung sollte Hintergrundinformationen liefern und die Bühne für den Rest des Beitrags bereiten. Der Hauptteil sollte Schlüsselpunkte abdecken und relevante Statistiken liefern. Der Schluss sollte die Schlüsselpunkte zusammenfassen und dem Leser eine Mitnahme bieten. Der Beitrag sollte mindestens 1000 Wörter und nicht mehr als 1500 Wörter umfassen, und der Ton sollte informativ, aber zugänglich sein.
```


#### Bericht:

```
Bitte schreibe einen Bericht über [Thema]. Dein Bericht sollte strukturiert, klar und prägnant sein. Die Informationen sollten in einer Liste mit Aufzählungspunkten oder in einer Tabelle organisiert sein. Die Gesamtwortzahl sollte nicht mehr als [x] Wörter betragen. Der Bericht sollte in die folgenden Abschnitte unterteilt sein, jeder mit einer maximalen Wortzahl von [y]: [Abschnitt 1], [Abschnitt 2], [Abschnitt 3] usw.
```

#### Produktbeschreibung:

```
Schreibe eine Produktbeschreibung für [Produktname], einschließlich Spezifikationen, Funktionen, Vorteilen und allen weiteren relevanten Informationen.
```

#### Fallstudie:

```
Schreibe eine Fallstudie über [Thema]. In dieser Fallstudie solltest du [Schlüsselpunkte] diskutieren. Stelle sicher, dass du [spezifische Informationen] einbeziehst. Verwende klare und prägnante Sprache.
```

```
Schreibe eine Fallstudie und eine Produktbeschreibungsseite für das gegebene Produkt. Die Fallstudie sollte eine Einleitung, Hintergrundinformationen, Problemstellung, Lösung und Ergebnisse enthalten. Die Produktbeschreibung sollte erläutern, was das Produkt ist und warum es erstellt wurde, welches Problem es löst, wie es das Problem löst und die Ergebnisse. Verwende klare und prägnante Sprache und biete Beispiele ähnlicher Seiten zur Orientierung des Modells. Der Ton sollte professionell und informativ sein.
```

#### Essay:

```
Schreibe einen gut strukturierten [Art des Essays] Essay basierend auf folgendem:

Thema: [Thema des Essays]

Verfahren:

Einleitung
Hintergrundinformationen
Hauptargumente
Beweise zur Unterstützung
Gegenargumente
Schlussfolgerung

Kontext:

Relevante Informationen zum Thema
Bedeutung des Themas
Zweck des Essays
```

Hinweis: Die Art des Essays kann erklärend, erzählend, argumentativ, beschreibend, vergleichend und kontrastierend, ursächlich und wirkend sein ...

#### Stellenbeschreibung:

```
Erstelle eine Stellenbeschreibung für [Jobtitel] bei [Ort/Firmenname]. Inkludiere Verantwortlichkeiten, Qualifikationen, Vergütungspaket, Unternehmenskultur basierend auf gegebenen Details:

[Liste mit Schlüsselwörtern zu Verantwortlichkeiten, Qualifikationen, Leitbild oder Vision ...]
```

```
Schreibe eine Stellenbeschreibung für [Jobtitel] bei [Firmenname] in [Ort]. Der Job ist in der [Branche] Branche und ist eine [Art des Jobs].
Bitte inkludiere Abschnitte für Jobverantwortlichkeiten, Jobanforderungen, Jobvorteile basierend auf folgenden Details:

[Beispiele, Schlüsselwörter]
```


#### Github-Readme-Seite

```
Schreibe ein detailliertes Github-Readme für ein neues Open-Source-Projekt. Das Readme sollte eine kurze, aber informative Beschreibung des Projekts, schrittweise Installationsanweisungen, klare Anwendungsbeispiele und gut definierte Beitragshinweise im Markdown-Format enthalten.
```

```
Generiere eine Github-README-Seite für das gegebene Projekt
[Projektdetails, verwandte Schlüsselwörter]
Verfahren:

Einleitung: Biete einen kurzen Überblick über das Projekt
Installation: Erkläre, wie das Projekt installiert und eingerichtet wird
Nutzung: Erkläre, wie das Projekt verwendet wird
Beiträge: Erkläre, wie man zum Projekt beitragen kann
Schlussfolgerung: Fasse das Projekt zusammen und biete zusätzliche Informationen.

```

```
Schreibe eine GitHub-Readme-Seite für ein Projekt. Die Readme-Seite sollte eine Einführung in das Projekt, dessen Zweck, Installationsanweisungen und Nutzungshinweise enthalten. Die Sprache sollte klar und prägnant sein.
```
