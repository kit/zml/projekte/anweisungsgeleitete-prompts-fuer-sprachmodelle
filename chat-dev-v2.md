#### Code-Umschreibung

```
Mensch: Ich möchte, dass du den folgenden Code-Schnipsel gemäß den folgenden Richtlinien umschreibst:

Das Ziel ist es, den Code lesbarer, wartbarer und robuster zu machen.

Richtlinien:

- Verwende aussagekräftigere Funktions- und Variablennamen
- Teile den Code in kleinere wiederverwendbare Funktionen auf
- Füge JSDoc-Kommentare hinzu, um Funktionen zu dokumentieren
- Verwende moderne JS-Features wie async/await statt Promises
- Füge Fehlerbehandlung hinzu, statt stillschweigend zu scheitern
- Folge einem konsistenten Code-Stil und Formatierung
- Exportiere eine saubere öffentliche API textToSpeech()
- Entferne unnötigen Code und Kommentare
- Verwende uuid statt benutzerdefinierter ID-Generierung
- Modularisiere die Audiogenerierung in separate Utility-Funktionen


<code>

</code>


Bitte setze deinen umgeschriebenen Code in <rewritten_code></rewritten_code> Tags.

Assistent: <rewritten_code>
```
---

> **Aufforderung zur Code-Umschreibung**
```
Mensch: Ich möchte, dass du den folgenden Computercode mit den folgenden Anweisungen umschreibst: "{{ANWEISUNGEN}}".

<code>
{{CODE_BLOCK}}
</code>

Bitte setze deinen umgeschriebenen Code in <rewritten_code></rewritten_code> Tags.

Assistent: <rewritten_code>
```
---

> **Aufforderung für Code-Fragen & Antworten**
```
Mensch: Ich werde dir einen Ausschnitt eines Computerprogramms geben und dir dann einige Fragen zu dem Code stellen.

<code_snippet>
{{CODE_TEXT}}
</code_snippet>

Hier ist die erste Frage: {{FRAGE}}

Assistent:
```
---

> **Aufforderung zur Code-Fehlerbehebung**
```
Mensch: Dir wird der folgende Code bereitgestellt.

<buggy_code>
{{CODE_BLOCK}}
</buggy_code>

Kannst du ihn identifizieren und eine Lösung vorschlagen? Bitte setze deinen korrigierten Code in <fixed_code></fixed_code> Tags.

Assistent: <fixed_code>
```
---

> **Aufforderung zur Code-Erklärung**
```
Mensch: Bitte erkläre mir den folgenden Code in einfachen Worten.

<code_to_explain>
{{CODE_SECTION}}
</code_to_explain>

Assistent:
```
---

> **Aufforderung zur Umwandlung von Code in Pseudocode**
```
Mensch: Wandle den folgenden Code in Pseudocode um.

<actual_code>
{{PROGRAM_CODE}}
</actual_code>

Bitte setze deinen Pseudocode in <pseudocode></pseudocode> Tags.

Assistent: <pseudocode>
```
---

> **Aufforderung zur Umwandlung von Pseudocode in Code**
```
Mensch: Ich habe hier etwas Pseudocode und Notizen. Ich möchte, dass du ihn in Python-Code umwandelst.

<pseudocode_input>
{{PSEUDOCODE_TEXT}}
</pseudocode_input>

Bitte setze deinen Python-Code in <python_code></python_code> Tags.

Assistent: <python_code>
```
---

> Code-Umschreibung
```
Mensch: Ich möchte, dass du den folgenden Code gemäß diesen Anweisungen umschreibst:

Anweisungen:
- Verwende moderne Syntax und Best Practices
- Verbessere die Lesbarkeit und Wartbarkeit des Codes
- Optimiere die Leistung

<code>

{{CODE}}

</code>

Bitte präsentiere deinen überarbeiteten Code innerhalb von <rewrite></rewrite> Tags.

Assistent: <rewrite>

{{REVISED_CODE}}

</rewrite>
```
---

> Code-Review
```
Mensch: Dir wird ein Code-Snippet gegeben, deine Aufgabe ist es, den Code zu überprüfen. Hier ist das Code-Snippet, das ich gerne überprüft hätte:


<code>
{{CODE}}
</code>

Bitte folge diesen Richtlinien:
- Weise auf Syntaxfehler hin
- Schlage Verbesserungen für die Lesbarkeit vor
- Identifiziere Sicherheitslücken

Setze deine Überprüfung in `<review></review>` Tags.

Assistent: `<review>`
```
---

> Debugging
```
Mensch: Ich habe ein Problem mit meinem Code. Hier ist die Fehlermeldung, die ich erhalte:

<error_message>
{{ERROR_MESSAGE}}
</error_message>

Bitte hilf mir, dieses Problem zu debuggen, indem du folgende Schritte befolgst:
- Identifiziere die möglichen Ursachen des Fehlers
- Schlage Lösungen zur Behebung vor

Setze deine Debugging-Schritte und Lösungen in `<debugging></debugging>` Tags.

Assistent: `<debugging>`
```
---

> API-Design
```
Mensch: Ich möchte, dass du eine API für eine Aufgabenverwaltungsanwendung entwirfst. Hier sind die Funktionen, die sie haben sollte:

- Aufgaben erstellen
- Aufgaben aktualisieren
- Aufgaben löschen
- Aufgaben auflisten

Bitte setze dein API-Design in `<api_design></api_design>` Tags.

Assistent: `<api_design>`
```
---

> Code-Umwandlung
```
Mensch: Ich habe ein Stück Code in Python geschrieben und möchte es in JavaScript umwandeln. Hier ist der Python-Code:

<python_code>
{{PYTHON_CODE}}
</python_code>

Bitte setze den umgewandelten JavaScript-Code in `<js_code></js_code>` Tags.

Assistent: `<js_code>`
```
---

### Testfallgenerierung
```
Mensch: Ich möchte, dass du Testfälle für die folgende Funktion generierst:

<function>
{{FUNCTION}}
</function>

Bitte setze die Testfälle in `<test_cases></test_cases>` Tags.

Assistent: `<test_cases>`
```
---

> Code-Dokumentation
```
Mensch: Ich habe ein Code-Snippet, das dokumentiert werden muss. Hier ist der Code:

<code>
{{CODE}}
</code>

Bitte füge Kommentare und Erklärungen hinzu, um den Code verständlich zu machen. Setze deinen dokumentierten Code in `<documented_code></documented_code>` Tags.

Assistent: `<documented_code>`
```
---

> Frontend-Komponentenentwurf
```
Mensch: Ich benötige eine Frontend-Komponente für eine Login-Seite. Hier sind die Anforderungen:

- Eingabefeld für E-Mail
- Eingabefeld für Passwort
- Absendebutton

Bitte setze deinen Frontend-Komponentenentwurf in `<component_design></component_design>` Tags.

Assistent: `<component_design>`
```
---
```
> DevOps-Aufgabe

Mensch: Ich möchte, dass du die Schritte zum Einrichten einer CI/CD-Pipeline für eine Node.js-Anwendung skizzierst. Bitte befolge diese Richtlinien:

- Verwende GitHub Actions für CI/CD
- Deploy auf AWS

Setze deine Schritte in `<devops_steps></devops_steps>` Tags.

Assistent: `<devops_steps>`
```
---