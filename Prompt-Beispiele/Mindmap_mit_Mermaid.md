
# Erstellen einer MindMap

Mit diesen Prompts kann eine Gliederung in eine grafische Darstellung in Form einer MindMap überführt werden. Der Trick besteht daraus das Sprachmodell die Regeln der ASCII-Syntax Mermaid für MindMaps beizubringen. Diese Syntax erzeugt Diagramme aus markdown-ähnlichem Text. Die Textausgabe kann per Copy&Paste auf [https://mermaid.live/](https://mermaid.live/) in ein Bild überführt werden. Dort findet sich auch die vollständige [Dokumentation](https://mermaid.js.org/syntax/mindmap.html) der Syntax. 

## Schritt 0: Beispielgliederung erstellen

**Prompt**:

```
Erstelle eine mit Markdown formatierte Gliederung zum Thema ….
```

## Schritt 1: Grundstruktur der MindMap erzeugen

**Prompt**:

```
Stelle den Inhalt des vorherigen Inhalts  als Mindmap dar. 
Dazu verwendest du die Mermaid Syntax:
 
1. Beginnen mit dem Keyword mindmap, um anzugeben, dass eine Mindmap erstellt wird.
2. Definieren einmal den Hauptknoten mit der Syntax root((Ihr Hauptthema)). Die doppelten Klammern (( )) repräsentieren einen Knoten.
3. Fügen die Unterthemen hinzu, indem du unter dem Hauptthema einrückst. Verwende dafür keine Aufzählungszeichen, Nummerierungen oder Pfeile wie -->, die Einrückung reicht aus.
4. Verzweigungen und weitere Unterthemen können durch weiteres Einrücken unter jedem Punkt hinzugefügt werden.
5. Fügen Inhalte mit spezifischen IDs hinzu und klammern den Text ein, um Markdown-Formatierungen wie Fett- oder Kursivschrift zu verwenden, umgebe den Text mit entsprechenden Markdown-Symbolen (** für fett, * für kursiv). Nutze diese Möglichkeit nur um besonders, wichtige Stichwörter zusätzlich hervorzuheben. Wenn aus dem Schritt vorher bereits eine Formatierung vorhanden ist, übernehme diese.
6. Beachten, dass der gesamte Text von Einträgen mit Markdown-Formatierung in Backticks (`) eingeschlossen sein muss. Z.B. id2["`The dog in **the** hog... a *very long text* that wraps to a new line`"].
```

## Schritt 2: Verschiedene Formen verwenden

**Prompt**:

```
Die so erhaltene Beschreibung der Struktur ergänzt du nun in einem weiteren Schritt:

Verwende ein Vielfalt an Formen, um die Mindmaps individuell und je nach Kontext 
besser erfassbar zu gestalten. Die Syntax zur Definition der Form eines Knotens 
verwendet eine Bezeichnung gefolgt von der Formdefinition. Unterstützte Formen 
umfassen:
* Quadrat: Durch die Angabe von bezeichnung_1[I am a square] wird ein Knoten als Quadrat dargestellt.
* Abgerundetes Quadrat: Mit bezeichnung_2(I am a rounded square) erhält man ein abgerundetes Quadrat.
* Kreis: bezeichnung_3((I am a circle)) stellt einen Knoten als Kreis dar.
* Bang: Eine ungewöhnliche Form, die mit bezeichnung_4))I am a bang(( erzeugt wird.
* Wolke: bezeichnung_5)I am a cloud( zeigt den Knoten in Wolkenform.
* Sechseck: Durch bezeichnung_6{{I am a hexagon}} wird ein Knoten als Sechseck dargestellt.
```

# Schritt 3: Emojis hinzufügen

**Prompt**:

```
Die so erhaltene Beschreibung der Struktur ergänzt du nun in einem weiteren Schritt:
 
Verwende auch Emojis um den Text der Einträge visuell zu verbessern. 
Um Emojis hinzuzufügen, fügen den Unicode des Emojis direkt in den Text des 
Knotens oder des Icon-Eintrags ein. Der Unicode eines Emojis kann durch eine 
Online-Suche nach "Emoji Unicode" gefunden werden.  Bei Einträgen mit Emojis 
muss der gesamte Text des Eintrags in Backticks (`) eingeschlossen sein.
```

# Schritt 4: Icons zu den Zweigen hinzufügen

**Prompt**:

```
Die so erhaltene Beschreibung der Struktur ergänzt du 
nun in einem weiteren Schritt:
 
Verwende auch Icons, um die Einträge der Mindmap visuell zu verbessern. 
Füge Icons jeweils als separaten Eintrag hinzu, indem du das Icon in der nächsten
Zeile direkt unter den entsprechenden Knotenpunkt einrückst.  Schreibe das Icon
niemals in der gleichen Zeile hinter den Eintrag. Wähle ein passende Icons aus der
Font Awesome oder Material Icons Library aus. Icons werden mit der 
Syntax ::icon(fa fa-icon-name) dargestellt.
```

*Optional*: Kann auch angegeben werden, dass dies nur eine bestimmte Hierarchiebene erfolgen soll. Z.B.: `Füge nur für die Einträge der zweiten Ebene der Hierarchie Icons hinzu`. 