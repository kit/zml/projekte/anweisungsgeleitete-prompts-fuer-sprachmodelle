# Ohne Einbezug von Internetquellen antworten

Copilot bezieht in den Antwortprozess Internetquellen mit ein. Die Anfrage (Prompt) wird auf Stichworte ausgewertet. Zu diesen Stichworten findet eine Internetsuche statt. Die Ergebnisse dieser Suche werden als Kontext zum Prompt gegeben, um eine Antwort mit dem LLM zu erzeugen. Dies kann positiv sein, da Antworten so nicht nur Informationen zum Trainingszeitpunkt des Modellls beinhalten. Es kann aber auch ungewollt sein, da die Internetrecherche beispielsweise einen zusätzlichen Bias einbringen kann. 

Mit einem Zusatz im Prompt kann dies in einem solchen Fall unterbunden werden:

`… Bitte beantworte dies, ohne aktuelle Quellen aus dem Internet einzubeziehen.`

Praktischer Nebeneffekt, da die Bearbeitungsdauer durch die Internet-Suche entfällt, wird die Antwort spürbar schneller generiert.